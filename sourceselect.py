from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtCore import Qt
from qgis.gui import QgsAbstractDataSourceWidget
from qgis.core import QgsProviderRegistry
from qgis.PyQt import uic
import os

class SourceSelect(QgsAbstractDataSourceWidget, QDialog):
    
    def __init__(self, parent, fl, widgetMode):
        super(QgsAbstractDataSourceWidget, self).__init__(parent, fl, widgetMode)
        super(QDialog, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "sourceselect.ui"), self)
        self.setupButtons(self.buttonBox)

        self.mFileWidget.setDialogTitle(self.tr("Open GDAL supported dataset"))
        self.mFileWidget.setFilter(QgsProviderRegistry.instance().fileRasterFilters())
        self.mFileWidget.fileChanged.connect(self.fileChanged)

    def fileChanged(self, path):
        self.enableButtons.emit(os.path.exists(path))

    def addButtonClicked(self):
        self.addRasterLayer.emit(
            self.mFileWidget.filePath(),
            os.path.basename(self.mFileWidget.filePath()),
            "traster")


if __name__=='__main__':
    from qgis.core import QgsApplication
    app = QgsApplication([], True)
    app.initQgis()
    d = RasterMeshSourceSelect(None, Qt.WindowFlags(), 0)
    d.mFileWidget.setFilePath('/home/vmo/gao/pluies_kister/grb/T55965_KISTER_AROME.2021070100.grb')
    d.exec_()

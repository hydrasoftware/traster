from .sourceselectprovider import SourceSelectProvider
from .provider import createProvider, Provider

from qgis.core import QgsProviderRegistry, QgsProviderMetadata
from qgis.gui import QgsGui

class Plugin:

    def initGui(self):
        QgsProviderRegistry.instance().registerProvider(
            QgsProviderMetadata(Provider.KEY, 'temporal raster', createProvider)) 	

        self.sourceSelectProvider = SourceSelectProvider()
        QgsGui.sourceSelectProviderRegistry().addProvider(self.sourceSelectProvider)

    def unload(self):
        QgsGui.sourceSelectProviderRegistry().removeProvider(self.sourceSelectProvider)

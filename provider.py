from qgis.PyQt.QtCore import QDateTime, Qt
from qgis.core import (
    QgsRasterDataProvider,
    QgsRectangle,
    QgsCoordinateReferenceSystem,
    QgsRasterBlock,
    QgsDataProvider,
    Qgis,
    QgsRasterBlockFeedback,
    QgsProviderRegistry,
    QgsRasterDataProviderTemporalCapabilities,
    QgsInterval,
    QgsDateTimeRange
    )
import sip
import numpy
from osgeo import gdal

class Provider(QgsRasterDataProvider):
    KEY = "traster"

    def __del__(self):
        #print("detroyed", self)
        pass

    def __init__(self, uri, options, flags):
        super(QgsRasterDataProvider, self).__init__(uri, options)
        self.decorated = QgsProviderRegistry.instance().createProvider('gdal', uri, options, flags)
        self.flags = flags

        ds = gdal.OpenEx(uri, gdal.OF_MULTIDIM_RASTER)
        rootGroup = ds.GetRootGroup()
        #groups = [a.GetName() for a in rootGroup.GetGroupNames()]
        dimensions = [d.GetName() for d in rootGroup.GetDimensions()]
        # TODO: check properly that 'TIME' is in dimensions
        assert 'TIME' in dimensions
        self.bands = {i+1: {"name":n} for i, n in enumerate(
            [n for n in rootGroup.GetMDArrayNames() if n not in ['X', 'Y', 'TIME']])}
        
        # datetime64 may not be the right choice, it'd nice to have several test datasets
        times = rootGroup.OpenMDArray('TIME').ReadAsArray().astype('datetime64[s]')
        self.times = times
        self.temporalCapabilities().setHasTemporalCapabilities(True)
        if len(times) > 1 and len(set(times[1:] - times[:-1]))==1:
            print('constant')
            dt = int((times[1]-times[0]).astype('timedelta64[ms]').astype('int64'))
            self.temporalCapabilities().setDefaultInterval(QgsInterval(dt/1000))
            self.temporalCapabilities().setAllAvailableTemporalRanges([
                QgsDateTimeRange(QDateTime.fromMSecsSinceEpoch(int(t.astype('datetime64[ms]').astype('int64')), Qt.UTC),
                                 QDateTime.fromMSecsSinceEpoch(int(t.astype('datetime64[ms]').astype('int64'))+dt, Qt.UTC))
                for t in times])
            self.temporalCapabilities().setAvailableTemporalRange(
                QgsDateTimeRange(QDateTime.fromMSecsSinceEpoch(int(times[0].astype('datetime64[ms]').astype('int64')), Qt.UTC),
                                 QDateTime.fromMSecsSinceEpoch(int(times[-1].astype('datetime64[ms]').astype('int64'))+dt, Qt.UTC)))
        else:
            self.temporalCapabilities().setAllAvailableTemporalRanges([
                QgsDateTimeRange(QDateTime.fromMSecsSinceEpoch(int(s.astype('datetime64[ms]').astype('int64'))),
                                 QDateTime.fromMSecsSinceEpoch(int(e.astype('datetime64[ms]').astype('int64'))))
                for s, e in zip(times[:-1], times[1:])] + [
                QgsDateTimeRange(QDateTime.fromMSecsSinceEpoch(int(times[-1].astype('datetime64[ms]').astype('int64'))))])
            self.temporalCapabilities().setlAvailableTemporalRange(
                QgsDateTimeRange(QDateTime.fromMSecsSinceEpoch(int(times[-1].astype('datetime64[ms]').astype('int64')))))

        # TODO fix the followinf to have correct band mapping
        bno = 1
        for no, b in self.bands.items():
            md = rootGroup.OpenMDArray(b["name"])
            b["bandNo"] = bno
            bno += 1 # TODO: check that the band matches the MDArray name


    def clone(self):
        cpy = Provider(self.dataSourceUri(), QgsDataProvider.ProviderOptions(), self.flags)
        cpy.copyBaseSettings(self)
        #print("cloned", self, 'to', cpy)
        sip.transferto(cpy, cpy) # this is not a class meant to be used python side
        return cpy

    def dataType(self, bandNo):
        return self.decorated.dataType(self.bands[bandNo]['bandNo'])

    def sourceDataType(self, bandNo):
        return self.decorated.dataType(self.bands[bandNo]['bandNo'])
    
    def extent(self):
        return self.decorated.extent()

    def htmlMetadata(self):
        return self.decorated.htmlMetadata()

    def lastError(self):
        return self.decorated.lastError()

    def lastErrorTitle(self):
        return self.decorated.lastErrorTitle()

    def isValid(self):
        return self.decorated.isValid()

    def name(self):
        return self.decorated.name()

    def crs(self):
        return self.decorated.crs()

    def description(self):
        return ("this provider rasterizes mesh data from MDAL supported formats, "
            "and additional raster (signle band) can be provided that is substracted to "
            "the rasterized mesh")
    
    def bandCount(self):
        return len(self.bands)

    def generateBandName(self, bandNo):
        return self.bands[bandNo]['name']

    def block(self, bandNo, extent, width, height, feedback=None):
        rg = self.temporalCapabilities().requestedTemporalRange()
        tb = rg.begin().toSecsSinceEpoch()
        idx =  (numpy.abs(self.times.astype('int64') - tb)).argmin()
        print(idx)
        band = (bandNo-1)*len(self.times) + idx + 1
        rb = self.decorated.block(band, extent, width, height, feedback)
        sip.transferto(rb, rb)
        return rb

    def xSize(self):
        return self.decorated.xSize()

    def ySize(self):
        return self.decorated.ySize()

    def xBlockSize(self):
        return self.decorated.xBlockSize()

    def yBlockSize(self):
        return self.decorated.yBlockSize()

    def stepWidth(self):
        return self.decorated.stepWidth()

    def stepHeight(self):
        return self.decorated.stepHeight()

    def capabilities(self):
        return self.decorated.capabilities()


def createProvider(uri, options, flags):
    return Provider(uri, options, flags)
    


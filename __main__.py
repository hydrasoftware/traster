from qgis.core import (
    QgsApplication,
    QgsProject,
    QgsMapSettings,
    QgsMapRendererCustomPainterJob,
    QgsProviderRegistry,
    QgsProviderMetadata,
    QgsRasterLayer,
    QgsVectorLayer,
    QgsRectangle,
    QgsPointXY,
    QgsRaster
    )
from qgis.gui import QgsGui
from qgis.PyQt.QtGui import QImage, QPainter, QColor
from qgis.PyQt.QtCore import QSize
import os

from .plugin import Plugin

app = QgsApplication([], True)
app.initQgis()

plugin = Plugin()
plugin.initGui()

layer = QgsRasterLayer('/home/vmo/gao/pluies_kister/grb/T55965_KISTER_AROME_PE.2021070921.grb', 'tata', 'traster')
assert layer.isValid()

layer = QgsRasterLayer('/home/vmo/gao/pluies_kister/grb/T55965_KISTER_AROME.2021070100.grb', 'toto', 'traster')
assert layer.isValid()



print(layer)

exit(0)

img = QImage(512, 512, QImage.Format_ARGB32_Premultiplied)

ms = QgsMapSettings()
ms.setLayers([layer])
ms.setExtent(layer.extent())
ms.setOutputSize(img.size())
print('scale', ms.scale())
img.setDevicePixelRatio(ms.scale())

p = QPainter()
p.begin(img)
p.setRenderHint(QPainter.Antialiasing)
render = QgsMapRendererCustomPainterJob(ms, p)
print("render")
render.start()
print("started")
render.waitForFinished()
print("finished")
p.end()

# save the image
img.save('/tmp/traster.png')

plugin.unload()

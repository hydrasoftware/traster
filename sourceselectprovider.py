from qgis.gui import QgsSourceSelectProvider
from qgis.PyQt.QtGui import QIcon
from .sourceselect import SourceSelect
from .provider import Provider
import os

class SourceSelectProvider(QgsSourceSelectProvider):

    def providerKey(self):
        return Provider.KEY

    def text(self):
        return "temporal raster"

    def ordering(self):
        return 0

    def icon(self):
        return QIcon(os.path.join(os.path.dirname(__file__), "traster.svg"))
    
    def createDataSourceWidget(self, parent, fl, widgetMode):
        return SourceSelect(parent, fl, widgetMode)



